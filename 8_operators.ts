interface Person {
    name: string
    age: number
}

type PersonKeys = keyof Person; // "name" | 'age'

let key: PersonKeys = 'name';
key = 'age';

type User = {
    _id: number
    name: string
    email: string
    createAt: Date
}

//создать тип на основе существующего исключив не нужные поля
type UserKeysNoMeta1 = Exclude<keyof User, '_id' | 'createAt'> //'name', 'email'
//взять определенные поля
type UserKeysNoMeta2 = Pick<User, 'name' | 'email'>
