class Typescript {
    version: string
    constructor(version: string) {
        this.version = version;
    }
    info(name: string): string {
        return `${name}: Typescript version is ${this.version}`
    }
}

// class Car {
//     readonly model: string
//     readonly numberOfWheels: number = 4
//
//     constructor(theModel: string) {
//         this.model = theModel
//     }
// }
// сокращенная запись
class Car {
    readonly numberOfWheels: number = 4
    constructor(readonly model: string) {
    }
}

// модификаторы доступа
class Animal {
    public voice: string = 'gav'
    public color: string = 'black'

    private go(): void {
        console.log('Go')
    }
}

class Cat extends Animal{
    public setVoice(voice: string): void {
        this.voice = voice;
    }
}

const cat = new Cat();
console.log(cat.voice);
cat.setVoice('myu');
console.log(cat.voice);

// ========================================
//абстрактные классы
//нельзя создавать объекты, только наследоваться
//аследники должны реализовать все абстрактные методы
//акже могут содержать поля и методы как и обычные классы
//(обязывает реализовывать определенные методы наследниками)

abstract class Component {
    abstract render(): void
    abstract info(): string
}

class AppComponent extends Component {
    render(): void {
        console.log('Component on render')
    }
    info(): string {
        return "This is info";
    }
}