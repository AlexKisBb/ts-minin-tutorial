const str: string = 'Hello';

const isFetching: boolean = true;
const isLoading: boolean = false;

const int: number = 42;
// int = '' - среда подсвечивает ошибку при попытке переопределения
const float: number = 4.2;
const num: number = 3e10;
// из экспоненциальной в десятичную
console.log(num.toFixed());

const message: string = 'Hello ts';

//два способа указания типа массива
const numberArray: number[] = [1, 1, 2, 3, 5, 8, 13];
const numberArray2: Array<number> = [1, 1, 2, 3, 5, 8, 13];

const words: string[] = ['Hello', 'ts'];

// tuple - массивы из разных типов данных
const contact: [string, number] = ['Alex', 333222];

// Any - переопределяемая переменная
let variable: any = 'New string';
variable = [];

// указание возвращаемого тиа функцией
function sayMyName(name: string): void {
    console.log(name)
}

// Never - тип значение которого никогда не происходит
// (ошибка, бесконечный цикл)
function throwError(message: string): never {
        throw new Error(message)
}

// создание собственного типа (alias - псевдоним)
type Login = string;
const login: Login = 'admin';

// тип который принимает значение различных типов
type ID = string | number;
const id1: ID = 1234;
const id2: ID = '1234';

// null undefined зачастую служат для определения типов
type someType = string | null | undefined;










