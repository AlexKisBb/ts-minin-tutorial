const arrayOfNumbers: Array<number> = [1, 2, 5, 10];
const arrayOfStrings: Array<string> = ['Hell', 'Alex'];

//общий тип
//казываем eneric тип который будет подстраиваться
// под передаваемый контент в массиве
function reverse<T>(array: T[]): T[] {
    return array.reverse()
}

reverse(arrayOfNumbers);
reverse(arrayOfStrings);

//generic в классе

class User<T, K extends number> {
    constructor(public name: T, public age: K) {
    }
    public getPass(): string {
        return `${this.name}${this.age}`
    }
    public getSecret(): number {
        return this.age**2
    }
}

//передаваемые типы контролируются, строку '20'передать нельзя
// const max = new User('Alex', '20');