var Membership;
(function (Membership) {
    Membership[Membership["Simple"] = 0] = "Simple";
    Membership[Membership["Standart"] = 1] = "Standart";
    Membership[Membership["Premium"] = 2] = "Premium";
})(Membership || (Membership = {}));
var membership = Membership.Standart;
var membershipReverse = Membership[2];
console.log(membership);
console.log(membershipReverse);
// смесь массива и объекта, обращение по
// ключу и значение, можно задавать свои индексы
var Directions;
(function (Directions) {
    Directions[Directions["UP"] = 2] = "UP";
    Directions[Directions["Down"] = 4] = "Down";
})(Directions || (Directions = {}));
console.log(Directions.UP);
console.log(Directions.Down);
var SocialMedia;
(function (SocialMedia) {
    SocialMedia["VK"] = "VK";
    SocialMedia["FB"] = "FACEBOOK";
    SocialMedia["INST"] = "INSTAGRAM";
})(SocialMedia || (SocialMedia = {}));
var social = SocialMedia.INST;
console.log(social);
