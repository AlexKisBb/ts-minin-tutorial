enum Membership {
    Simple,
    Standart,
    Premium
}

const  membership = Membership.Standart;
const membershipReverse = Membership[2];
console.log(membership);
console.log(membershipReverse);

// смесь массива и объекта, обращение по
// ключу и значение, можно задавать свои индексы
// внятные имена для индексов
enum Directions {
    UP = 2,
    Down = 4
}
console.log(Directions.UP);
console.log(Directions.Down);

enum  SocialMedia {
    VK = 'VK',
    FB = 'FACEBOOK',
    INST = 'INSTAGRAM'
}
const social = SocialMedia.INST;
console.log(social);

