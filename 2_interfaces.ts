interface Rect {
    readonly id: string // только для чтения
    color?: string      //? необязательный параметр
    size: {
        width: number
        height: number
    }
}

const rect1: Rect = {
    id: '1234',
    size: {
        width: 20,
        height: 10
    }
}

const rect2: Rect = {
    id: '1234',
    size: {
        width: 40,
        height: 50
    }
}
rect2.color = 'black';

// к какому типу будет относиться объект
const rect3 = {} as Rect;
const rect4 = <Rect>{};

//==========================================
//наследование интерфейсов
interface RectWithArea extends  Rect {
    getArea: () => number
}

const rect5: RectWithArea = {
    id: '123',
    size: {
        width: 20,
        height: 40
    },
    getArea(): number {
        return this.id.size.width * this.size.height;
    }
}
// взаимодействие с классами
interface IClock {
    time: Date
    setTime(date: Date): void
}

class  Clock implements IClock {
    time: Date = new Date();
    setTime(date: Date) {
        this.time = date;
    }
}

// индексируемые типы (указываем какого типа может быть ключи и значения)
interface Styles {
    [key: string]: string
}

const css: Styles = {
    border: '1px solid black',
    marginTop: '2px',
    borderRadius: '5px'
}
